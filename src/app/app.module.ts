import { PopupAlertService } from './services/popup-alert.service';
import { PopupAlertModule } from './components/popup-alert/popup-alert.module';
import { UserModule } from './views/user/user.view.module';
import { EditUserModule } from './views/edit-user/edit-user.view.module';
import { appRouting } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http'

import { AppComponent } from './app.component';
import { NewUserModule } from './views/new-user/new-user.module';
import { UsersModule } from './views/users/users.view.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UsersModule,
    NewUserModule,
    EditUserModule,
    UserModule,
    PopupAlertModule,
    HttpModule,
    appRouting
  ],
  providers: [
    PopupAlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
