import { Observable } from 'rxjs/Observable';
import { User } from './../models/User';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

    url = 'http://jsonplaceholder.typicode.com/users';

    constructor(private http: Http) { }

    getUsers(): Observable<User[]> {
        return this.http.get(this.url).map(response => response.json());
    }

    getUserById(id: number): Observable<User> {
        return this.http.get(`${this.url}/${id}`).map(response => response.json());
    }

    createUser(user: User): Observable<User> {
        return this.http.post(this.url, user).map(response => response.json());
    }

    deleteUser(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }

    editUser(user: User) {
        return this.http.patch(`${this.url}/${user.id}`, user);
    }
}