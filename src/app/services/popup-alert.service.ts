import { BehaviorSubject ,Observable } from 'rxjs/Rx';
import { PopupAlertData } from './../models/PopupAlertData';
import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class PopupAlertService{

    private alerts$ = new BehaviorSubject([]);
    private alerts: PopupAlertData[] = [];

    getAlerts(): Observable<PopupAlertData[]> {
        return this.alerts$.asObservable();
    }

    createAlert(alert: PopupAlertData) {
        this.alerts = [...this.alerts, alert];
        this.alerts$.next(this.alerts);
    }
}