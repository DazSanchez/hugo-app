import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/User';

@Pipe({
    name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {
    transform(userArray: User[], value?: string): User[] {
        if(!userArray) return null;
        if(value == null) return userArray;
        return userArray.filter(user => user.name.toLowerCase().includes(value));
    }
}