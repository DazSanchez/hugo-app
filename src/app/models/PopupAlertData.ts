export interface PopupAlertData {
    message: string,
    type: string,
    closed?: boolean
}