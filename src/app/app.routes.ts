import { UserComponent } from './views/user/user.view.component';
import { EditUserComponent } from './views/edit-user/edit-user.view.component';
import { NewUserComponent } from './views/new-user/new-user.view.component';
import { UsersComponent } from './views/users/users.view.component';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'users/new', component: NewUserComponent },
  { path: 'users/:id/edit', component: EditUserComponent },
  { path: 'user/:id', component: UserComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'users' }
];

export const appRouting = RouterModule.forRoot(routes);
