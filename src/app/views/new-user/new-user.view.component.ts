import { PopupAlertService } from './../../services/popup-alert.service';
import { User } from '../../models/User';
import { UserService } from './../../services/user.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'new-user',
    templateUrl: './new-user.view.component.html'
})
export class NewUserComponent {

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: PopupAlertService) {}

    onSave(user: User) {
        this.userService.createUser(user)
            .subscribe(response => {
                this.router.navigate(['/users', 'created'], { queryParams: { status: 'created' } });
                this.alertService.createAlert({
                    message: 'User created succesfully.',
                    type: 'info'
                });
            });
    }
}