import { MaterialIconModule } from './../../components/material-icon/material-icon.module';
import { UserFilterPipe } from './../../pipes/user-filter.pipe';
import { FormsModule } from '@angular/forms';
import { UserService } from './../../services/user.service';
import { UsersComponent } from './users.view.component';
import { UserTableModule } from '../../components/user-table/user-table.module';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        UserTableModule,
        FormsModule,
        MaterialIconModule
    ],
    declarations: [UsersComponent, UserFilterPipe],
    exports: [UsersComponent],
    providers: [UserService]
})
export class UsersModule{}