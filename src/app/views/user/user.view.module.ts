import { MaterialIconModule } from './../../components/material-icon/material-icon.module';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UserComponent } from './user.view.component';
import { UserService } from '../../services/user.service';
import { NgModule } from "@angular/core";

@NgModule({
    imports: [CommonModule, RouterModule, MaterialIconModule],
    declarations: [UserComponent],
    providers: [UserService],
    exports:[UserComponent]
})
export class UserModule{}