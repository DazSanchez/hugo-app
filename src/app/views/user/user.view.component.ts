import { UserService } from './../../services/user.service';
import { User } from '../../models/User';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'user',
    templateUrl: './user.view.component.html'
})
export class UserComponent implements OnInit{
    user: User;

    constructor(private route: ActivatedRoute, private userService: UserService) {}
    
    ngOnInit() {
        let id:number = +this.route.snapshot.paramMap.get('id');
        this.userService.getUserById(id).subscribe(user => this.user = user);
    }
}