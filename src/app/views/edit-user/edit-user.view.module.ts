import { CommonModule } from '@angular/common';
import { UserService } from './../../services/user.service';
import { UserFormModule } from '../../components/user-form/user-form.module';
import { NgModule } from "@angular/core";
import { EditUserComponent } from "./edit-user.view.component";

@NgModule({
    imports: [CommonModule, UserFormModule],
    declarations: [EditUserComponent],
    exports: [EditUserComponent],
    providers: [UserService]
})
export class EditUserModule{}