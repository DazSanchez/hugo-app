import { PopupAlertService } from './../../services/popup-alert.service';
import { User } from './../../models/User';
import { Observable } from 'rxjs/Observable';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'edit-user',
    templateUrl:'./edit-user.view.component.html'
})
export class EditUserComponent implements OnInit {

    private user$: Observable<User>;

    constructor(
        private userService: UserService,
        private alertService: PopupAlertService,
        private route: ActivatedRoute,
        private router: Router) {}

    ngOnInit() {
        let id: number = +this.route.snapshot.paramMap.get('id');
        this.user$ = this.userService.getUserById(id);
    }

    onSave(user: User) {
        this.userService.editUser(user)
            .subscribe(() => {
                this.alertService.createAlert({
                    message: 'User successfully edited!',
                    type: 'info'
                });
                this.router.navigate(['/users']);
            });
    }
}