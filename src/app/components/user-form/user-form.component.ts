import { Component, Output, EventEmitter, Input, OnInit, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/User';

@Component({
    selector: 'user-form',
    templateUrl: './user-form.component.html'
})
export class UserFormComponent implements OnChanges {
    @Input() user: User;

    @Output() save = new EventEmitter();

    private form: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.form = formBuilder.group({
            name: ['', Validators.required],
            username: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            address: formBuilder.group({
                street: '',
                city: ''
            })
        });
    }

    ngOnChanges() {
        if(!this.user) return;

        this.form.patchValue(this.user);
    }

    onSave() {
        let newUser = { ... this.user , ... this.form.value };
        this.save.emit(newUser);
    }

    get nameFormControl() { return this.form.get('name'); }
    get usernameFormControl() { return this.form.get('username'); }
    get emailFormControl() { return this.form.get('email'); }
} 