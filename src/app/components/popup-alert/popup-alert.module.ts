import { MaterialIconModule } from './../material-icon/material-icon.module';
import { PopupAlertService } from './../../services/popup-alert.service';
import { CommonModule } from '@angular/common';
import { PopupAlertComponent } from './popup-alert.component';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [CommonModule, MaterialIconModule],
    declarations: [PopupAlertComponent],
    exports: [PopupAlertComponent],
    providers: [PopupAlertService]
})
export class PopupAlertModule {}