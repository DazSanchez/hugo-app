import { Observable } from 'rxjs/Observable';
import { PopupAlertData } from '../../models/PopupAlertData';
import { PopupAlertService } from './../../services/popup-alert.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'popup-alert',
    templateUrl: './popup-alert.component.html'
})
export class PopupAlertComponent implements OnInit {

    private alerts$: Observable<PopupAlertData[]>;

    constructor(private alertService: PopupAlertService) {}

    ngOnInit() {
        this.alerts$ = this.alertService.getAlerts();
    }
}