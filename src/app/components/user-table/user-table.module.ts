import { MaterialIconModule } from './../material-icon/material-icon.module';
import { NgModule } from '@angular/core';
import { UserTableComponent } from './user-table.component';
import { CommonModule } from '@angular/common';
import { UserTableRowComponent } from './user-table-row.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MaterialIconModule
    ],
    declarations: [
        UserTableComponent,
        UserTableRowComponent
    ],
    exports: [UserTableComponent]
})
export class UserTableModule {}