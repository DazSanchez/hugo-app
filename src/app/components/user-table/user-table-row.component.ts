import { User } from '../../models/User';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: '[user-table-row]',
    templateUrl: './user-table-row.component.html'
})
export class UserTableRowComponent {
    @Input('user-table-row') user: User;
    
    @Output() delete = new EventEmitter();

    onDelete() {
        this.delete.emit(this.user.id);
    }
}