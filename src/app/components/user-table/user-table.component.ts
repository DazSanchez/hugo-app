import { User } from '../../models/User';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'user-table',
    templateUrl: './user-table.component.html'
})
export class UserTableComponent {
    @Input() users: User[];
    
    @Output() delete: EventEmitter<Number> = new EventEmitter();

    onDelete(id: number) {
        this.delete.emit(id);
    }
}